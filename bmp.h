
#include <stdio.h>
#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "image.h"
#include "util.h"

struct  __attribute__((packed)) bmp_header
{
uint16_t bfType;
uint32_t  bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t  biHeight;
uint16_t  biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t  biClrImportant;
};




bool read_header_from_file( const char* filename, struct bmp_header* header );
enum read_status from_bmp( FILE* input, struct image* image );
enum read_status open_file(const char* filename,struct image* image,enum fileformat fileformat);
enum write_status save_file( const char* filename, struct image* image);
enum write_status to_bmp( FILE* output, struct image const* image );
struct bmp_header create_bmp_header(struct image const* image);
enum fileformat read_format(const char * filename);
#endif