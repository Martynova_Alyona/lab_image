CC=gcc
CFLAGS=-std=c18 -pedantic -Wall -Werror

all: Lab_image

bmp.o: bmp.c
		$(CC) -c $(CFLAGS) $< -o $@

image.o: image.c
		$(CC) -c $(CFLAGS) $< -o $@

util.o: util.c
		$(CC) -c $(CFLAGS) $< -o $@

main.o: main.c
		$(CC) -c $(CFLAGS) $< -o $@

Lab_image: bmp.o image.o util.o main.o
		$(CC) -o Lab_image $^

clean:
	rm -f bmp.o image.o util.o main.o Lab_image